package com.example.shoppingdemo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.shoppingdemo.splintr_pay_data.Address;
import com.example.shoppingdemo.splintr_pay_data.Customer;
import com.example.shoppingdemo.splintr_pay_data.History;
import com.example.shoppingdemo.splintr_pay_data.Item;
import com.example.shoppingdemo.splintr_pay_data.SplintrCheckout;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Checkout extends AppCompatActivity {

    private static final String PAD = "pdp", PII = "ibp";

    //Your public store key
    private static final String PUBLIC_STORE_KEY = "92D969B5-0B94-4329-9DF1-805E10126C90";

    private ImageView productImg;
    private TextView productTitle, productPrice, productRef, productDesc;
    private TextInputEditText contactFullName, contactEmail, contactPhoneNo, contactAddress;
    private Button continueWithSplintr, autoComplete;
    private LinearLayout padBtnView, piiBtnView;
    private MaterialRadioButton padRB, piiRB;
    private JSONObject productDetail;
    private String selectedPayType = PAD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        productImg = findViewById(R.id.checkout_product_img);
        productTitle = findViewById(R.id.checkout_product_name);
        productPrice = findViewById(R.id.checkout_product_price);
        productRef = findViewById(R.id.checkout_product_ref);
        productDesc = findViewById(R.id.checkout_product_description);
        contactFullName = findViewById(R.id.checkout_contact_name);
        contactEmail = findViewById(R.id.checkout_contact_email);
        contactPhoneNo = findViewById(R.id.checkout_contact_phone);
        contactAddress = findViewById(R.id.checkout_contact_address);
        continueWithSplintr = findViewById(R.id.checkout_splintr_pay_btn);
        autoComplete = findViewById(R.id.checkout_auto_fill_btn);
        padBtnView = findViewById(R.id.checkout_pad_btn);
        piiBtnView = findViewById(R.id.checkout_pii_btn);
        padRB = findViewById(R.id.checkout_pad_rb);
        piiRB = findViewById(R.id.checkout_pii_rb);

        Glide.with(this).load(getIntent().getStringExtra("image")).into(productImg);
        productTitle.setText(getIntent().getStringExtra("title"));
        productPrice.setText(String.format("%s AED", getIntent().getStringExtra("price")));

        autoComplete.setOnClickListener(view -> handleOnAutoCompleteForm());
        continueWithSplintr.setOnClickListener(view -> handleOnContiWithSplintr());

        productDetail = getProductDetail(getIntent().getIntExtra("id", 0));
        if (productDetail != null) {
            try {
                productRef.setText(String.format("Ref#. %s", productDetail.getString("reference_id")));
                productDesc.setText(productDetail.getString("description"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        padBtnView.setOnClickListener(v -> {
            handleOnPayOptionClick(PAD);
        });
        piiBtnView.setOnClickListener(v -> {
            handleOnPayOptionClick(PII);
        });
    }

    private void handleOnAutoCompleteForm() {
        contactFullName.setText("Test Account");
        contactEmail.setText("testaccount@splintrit.com");
        contactPhoneNo.setText("971123456789");
        contactAddress.setText("Dubai, United Arab Emirates");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private JSONObject getProductDetail(int id) {
        JSONObject productData = null;
        try {
            JSONObject jsonObject = new JSONObject(Utils.loadJSONFromAsset(this, "dummy_products"));
            JSONArray products = jsonObject.getJSONArray("products");
            for (int i = 0; i < products.length(); i++) {
                JSONObject product = products.getJSONObject(i);
                if (product.getInt("id") == id) {
                    productData = product;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productData;
    }

    private void handleOnContiWithSplintr() {
        String name = contactFullName.getText().toString();
        String email = contactEmail.getText().toString();
        String phone = contactPhoneNo.getText().toString();
        String address = contactAddress.getText().toString();

        if (name.isEmpty() || email.isEmpty() || phone.isEmpty() || address.isEmpty()) {
            Toast.makeText(this, "Please fill all the contact fields", Toast.LENGTH_SHORT).show();
            return;
        }

        SplintrCheckout splintrCheckout = new SplintrCheckout();
        splintrCheckout.store_public_key = PUBLIC_STORE_KEY; // Public store key of your merchant account on Splintr
        splintrCheckout.reference_id = String.valueOf(Utils.genRandomNum()); //Unique reference for the order on your system could be the Order Id
        splintrCheckout.currency = "AED";
        splintrCheckout.description = "Demo Android App Checkout";
        splintrCheckout.product_type = selectedPayType;

        Customer customer = new Customer();
        customer.name = name;
        customer.email = email;
        customer.phone = phone;

        Address cusAddress = new Address();
        cusAddress.city = "Dubai";
        cusAddress.country = "UAE";
        cusAddress.line_1 = address;

        History cusHistory = new History();
        cusHistory.gender = "F";
        cusHistory.dob = "1993-01-01";
        cusHistory.loyalty_score = "7";
        cusHistory.wishlist_count = "2";
        cusHistory.registered_since = "2018-08-01";

        customer.address = cusAddress;
        customer.history = cusHistory;
        splintrCheckout.customer = customer;

        List<Item> items = new ArrayList<>();
        Item item = new Item();
        try {
            item.reference_id = productDetail.getString("reference_id");
            item.unit_price = productDetail.getString("unit_price");
            item.description = productDetail.getString("description");
            item.image_url = productDetail.getString("image_url");
            item.product_url = productDetail.getString("product_url");
            item.title = productDetail.getString("title");
            item.quantity = "1";
        } catch (JSONException e) {
            e.printStackTrace();
        }

        items.add(item);
        splintrCheckout.items = items;

        double totalBill = Integer.parseInt(item.quantity) * Double.parseDouble(item.unit_price);

        //this url will be called at the end right after the order completion by Splintr
        splintrCheckout.callback_url = "https://splintr.xyz/completed";
        splintrCheckout.amount = totalBill;
        splintrCheckout.shipping_amount = 100;
        splintrCheckout.tax_amount = 10;
        splintrCheckout.discount_amount = 0;

        Gson gson = new Gson();
        String json = gson.toJson(splintrCheckout);

        Log.d("Splintr Checkout Data", json);

//        Toast.makeText(this, json, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Checkout.this, CheckoutPluginWebView.class);
        intent.putExtra("_checkoutData", json);
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (resultCode == RESULT_OK) {
                //Here add your logic for the checkout completed by Splintr
                String refId = data.getStringExtra("inquiryId");
                Toast.makeText(this, "Payment Completed, " + refId, Toast.LENGTH_SHORT).show();
                finish();
            } else {
                //Checkout failed by some reason
                Toast.makeText(this, "Payment Cancelled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void handleOnPayOptionClick(String type) {
        selectedPayType = type;
        if (type.equals(PAD)) {
            padRB.setChecked(true);
            piiRB.setChecked(false);
        } else if (type.equals(PII)) {
            padRB.setChecked(false);
            piiRB.setChecked(true);
        }
    }
}