package com.example.shoppingdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckoutPluginWebView extends AppCompatActivity {

    private static final String LOG_TAG = "PluginWebView";
    private static final String CREATE_CHECKOUT_URL = "https://linked.splintr.xyz/api/v1/merchant-estore/checkout";
    private static final String CHECKOUT_PLUGIN_URL = "https://react.splintr.xyz/checkout-process";

    private WebView pluginWebView;
    private String payRespCallbackURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_plugin_web_view);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        setTitle("Splintr Checkout");

        pluginWebView = findViewById(R.id.cpwv_webview);
        String checkoutData = getIntent().getStringExtra("_checkoutData");
        if (checkoutData != null) {
            submitData(checkoutData);
        }
        //Clear the web browser storage everytime
        WebStorage.getInstance().deleteAllData();

        pluginWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Log.d(LOG_TAG, "Url Load==> " + url);
                if (url.startsWith(payRespCallbackURL)) {
                    Uri uri = Uri.parse(url);
                    String transaction = uri.getQueryParameter("transaction");
                    boolean isSuccess = !TextUtils.isEmpty(transaction) && transaction.equals("success");
                    if (isSuccess) {
                        Intent intent = new Intent();
                        intent.putExtra("inquiryId", uri.getQueryParameter("inquiryId"));
                        setResult(RESULT_OK, intent);
                    } else {
                        setResult(RESULT_CANCELED);
                    }
                    finish();
                }
            }
        });
        WebSettings webSettings = pluginWebView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void submitData(String bodyParams) {
        try {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Please Wait...");
            progressDialog.show();

            JSONObject jsonBody = new JSONObject(bodyParams);
            payRespCallbackURL = jsonBody.getString("callback_url");
            Log.d(LOG_TAG, payRespCallbackURL);
            JsonRequest jsonRequest = new JsonObjectRequest(CREATE_CHECKOUT_URL, jsonBody,
                    response -> {
                        Log.d(LOG_TAG, response.toString());
                        progressDialog.cancel();
                        try {
                            if (response.getBoolean("success")) {
                                JSONObject data = response.getJSONObject("data");
                                String urlToOpen = String.format("%s/%s",
                                        CHECKOUT_PLUGIN_URL, data.getString("token"));
                                Log.d(LOG_TAG, urlToOpen);
                                pluginWebView.loadUrl(urlToOpen);
                            } else {
                                showMessage("Validation Error", response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showMessage("JSON Fromat Error", e.getLocalizedMessage());
                        }
                    },
                    error -> {
                        progressDialog.cancel();
                        Log.d(LOG_TAG, error.getLocalizedMessage());
                        showMessage("Error Occurred", error.getLocalizedMessage());
                    }
            );
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(jsonRequest);
        } catch (JSONException e) {
            e.printStackTrace();
            showMessage("Error Occurred", e.getLocalizedMessage());
        }
    }

    private void showMessage(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setNegativeButton("OK", (dialogInterface, i) -> {
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}