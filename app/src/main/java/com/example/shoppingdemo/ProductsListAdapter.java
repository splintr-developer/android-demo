package com.example.shoppingdemo;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

public class ProductsListAdapter extends RecyclerView.Adapter<ProductsListAdapter.ProductItemVH> {

    private Context context;
    private List<ProductData> productsList;

    public ProductsListAdapter(Context context, List<ProductData> productsList) {
        this.context = context;
        this.productsList = productsList;
    }

    @NonNull
    @Override
    public ProductItemVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_item_view, parent, false);
        return new ProductItemVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductItemVH holder, int position) {
        ProductData productData = productsList.get(position);
        Glide.with(context).load(productData.getImage()).into(holder.productImg);
        holder.productTitle.setText(productData.getTitle());
        holder.productPrice.setText(String.format("%s AED", productData.getPrice()));
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }

    class ProductItemVH extends RecyclerView.ViewHolder {

        private final ImageView productImg;
        private final TextView productTitle;
        private final TextView productPrice;

        public ProductItemVH(@NonNull View itemView) {
            super(itemView);
            productImg = itemView.findViewById(R.id.product_img);
            productTitle = itemView.findViewById(R.id.product_title);
            productPrice = itemView.findViewById(R.id.product_price);

            Button addToCartBtn = itemView.findViewById(R.id.product_add_to_cart_btn);
            addToCartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ProductData productData = productsList.get(getLayoutPosition());
                    Intent intent = new Intent(context, Checkout.class);
                    intent.putExtra("id", productData.getId());
                    intent.putExtra("title", productData.getTitle());
                    intent.putExtra("price", productData.getPrice());
                    intent.putExtra("image", productData.getImage());
                    context.startActivity(intent);
                }
            });
        }
    }
}
