package com.example.shoppingdemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView productsListRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        productsListRV = findViewById(R.id.products_list_rv);
        loadProducts();
    }

    private void loadProducts() {
        List<ProductData> productsDataList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(Utils.loadJSONFromAsset(this, "dummy_products"));
            JSONArray products = jsonObject.getJSONArray("products");
            for (int i = 0; i < products.length(); i++) {
                JSONObject product = products.getJSONObject(i);
                productsDataList.add(new ProductData(
                        product.getInt("id"),
                        product.getString("title"),
                        product.getString("unit_price"),
                        product.getString("image_url")
                ));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ProductsListAdapter productsListAdapter = new ProductsListAdapter(MainActivity.this, productsDataList);
        productsListRV.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        productsListRV.setAdapter(productsListAdapter);
    }
}