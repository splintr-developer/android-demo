package com.example.shoppingdemo.splintr_pay_data;

import java.util.List;

public class SplintrCheckout {
    public String store_public_key;
    public String reference_id;
    public String product_type;
    public String currency;
    public String callback_url;
    public double amount;
    public double shipping_amount;
    public double tax_amount;
    public double discount_amount;
    public String description;
    public Customer customer;
    public List<Item> items;
    public List<OrdersHistory> orders_history;
}
