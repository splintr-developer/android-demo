package com.example.shoppingdemo.splintr_pay_data;

import java.util.List;

public class OrdersHistory {
    public String amount;
    public String payment_method;
    public String ordered;
    public String captured;
    public String shipped;
    public String refunded;
    public String description;
    public String purchased_at;
    public List<Item> items;
    public String address;
}
