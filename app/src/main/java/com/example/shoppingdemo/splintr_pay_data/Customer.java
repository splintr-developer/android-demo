package com.example.shoppingdemo.splintr_pay_data;

public class Customer {
    public String email;
    public String phone;
    public String name;
    public Address address;
    public History history;
}
