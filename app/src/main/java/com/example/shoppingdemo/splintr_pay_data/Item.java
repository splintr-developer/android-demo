package com.example.shoppingdemo.splintr_pay_data;

public class Item {
    public String quantity;
    public String reference_id;
    public String title;
    public String unit_price;
    public String image_url;
    public String product_url;
    public String description;
}
